import glob
import csv
label=[]
file_name=[]
file_name_label={}
for fname in sorted(glob.glob('test/*.png')):
    temp_label=fname.split('_')[1]
    file_name_label[temp_label]=fname

with open('labels.txt','w') as temp:
    for key,value in file_name_label.items():
        temp.write(str(value)+" "+str(key)+"\n")